package org.sunspotworld;

import com.sun.spot.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ISwitch;
import com.sun.spot.resources.transducers.ISwitchListener;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.ITriColorLEDArray;
import com.sun.spot.resources.transducers.SwitchEvent;
import com.sun.spot.service.BootloaderListenerService;
import com.sun.spot.util.Utils;
import javax.microedition.io.*;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
 
/**
 *
 * @author Babblebase
 */
public class SwitchMonitor extends MIDlet implements ISwitchListener
{
    //radio setup
    private static final int HOST_PORT = 67;
    private RadiogramConnection radioConn = null;
    private Datagram datagram = null;
    private String spotAddress = System.getProperty("IEEE_ADDRESS");
    private static final int SAMPLE_PERIOD = 10*100;
    //switch variables/monitors
    private ISwitch sw1 = (ISwitch) Resources.lookup(ISwitch.class,"SW1");
    private ISwitch sw2 = (ISwitch) Resources.lookup(ISwitch.class,"SW2");
   
    //leds 
    ITriColorLEDArray leds = (ITriColorLEDArray)Resources.lookup(ITriColorLEDArray.class);
    
    
    /**
     * innitialise switch listeners
     */
    public SwitchMonitor()
    {
        sw1.addISwitchListener(this);
        sw2.addISwitchListener(this);
    }
    /**
     * start spot switch monitor application
     * @throws MIDletStateChangeException
     */
    protected void startApp() throws MIDletStateChangeException
    {
        System.out.println("Starting switch monitor applicaon on " + spotAddress
        + "...");
        ITriColorLED led = leds.getLED(0);
        led.setRGB(0, 0, 250);
        led.setOn();
        Utils.sleep(500);
        led.setOff();
        //start usb listener
        //listens for commands from pc.
        //BootloaderListenerService.getInstance().start();
        new com.sun.spot.service.BootloaderListenerService().getInstance().start();
       
        try
        {
            //opens broadcast connection to host port where 'desktop application'
            //is listening
            radioConn = (RadiogramConnection) Connector.open("radiogram://broadcast:" + HOST_PORT);
            //only send 12 bytes
            datagram = radioConn.newDatagram(50);
        }
        catch(Exception e)
        {
            System.err.println("Caught " + e +"in connection initialize");
            notifyDestroyed();
        }
    }
   
 
public void switchPressed(SwitchEvent evt)
{
        ITriColorLED led = leds.getLED(1);
        System.out.println("switch pressed");
        //records and sends data when a switch is pressed to 'Desktop app'
        long now = System.currentTimeMillis();
        String switchPressed = "SW1";
        if(evt.getSwitch()==sw2)
        {
            switchPressed = "SW2";
        }
        try
        {
            led.setRGB(0, 0, 250);
            datagram.reset();
            //records time of switch press
            datagram.writeLong(now);
            //records ID of switchpressed
            datagram.writeUTF(switchPressed);
            radioConn.send(datagram);

            led.setRGB(0, 255, 0);
            led.setOn();
            Utils.sleep(SAMPLE_PERIOD-(System.currentTimeMillis()-now));
            //leds.getLED(0).setOff();
            led.setOff();
           
        }
        catch(Exception e)
        {
            System.out.println("unable to read/send switch data!");
        }
    }
    public void switchReleased(SwitchEvent evt) {
        ITriColorLED led = leds.getLED(2);
        led.setRGB(250, 0, 0);
        led.setOn();
        Utils.sleep(1000);
        led.setOff();
    }
   
    protected void pauseApp() {
       
    }
 
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
       
    }
}